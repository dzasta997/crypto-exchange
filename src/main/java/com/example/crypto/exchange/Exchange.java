package com.example.crypto.exchange;

public class Exchange implements Runnable{

    private float rate;
    private float amount;
    private float result;
    private float fee;
    private final float feePercentage = 0.01f;

    public Exchange(float rate, float amount){
        this.amount = amount;
        this.rate=rate;
    }

    public float getRate() { return rate; }

    public void setRate(float rate) { this.rate = rate; }

    public float getAmount() { return amount; }

    public void setAmount(float amount) { this.amount = amount; }

    public float getResult() { return result; }

    public void setResult(float result) { this.result = result; }

    public float getFee() { return fee; }

    public void setFee(float fee) { this.fee = fee; }

    public float calculateFee(){
        return rate*amount*feePercentage;
    }

    public float calculateResult(){
        return rate*amount+calculateFee();
    }

    public void run(){
        setFee(calculateFee());
        setResult(calculateResult());
    }


}
