package com.example.crypto.exchange;

import com.example.crypto.coin.Coin;

import java.util.HashMap;

import static com.example.crypto.coin.CoinApi.getCoin;

public class ExchangeResponse {
    private String from;
    private HashMap<String, Exchange> exchanges = new HashMap();

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public HashMap<String, Exchange> getExchange() {
        return exchanges;
    }

    public void setExchange(HashMap<String, Exchange> exchange) {
        this.exchanges = exchange;
    }

    public  void calculateExchange(ExchangeRequest exchangeRequest){

        this.from = exchangeRequest.getFrom();
        Coin coin = getCoin(this.from);

        for(String baseCurrency: exchangeRequest.getTo()){
            Float rate = Float.parseFloat(coin.getRates().get(baseCurrency).getRate());
            Exchange exchange = new Exchange(rate, exchangeRequest.getAmount());
            Thread exchangeThread = new Thread(exchange);
            exchangeThread.start();
            exchanges.put(baseCurrency, exchange);
        }

    }

}
