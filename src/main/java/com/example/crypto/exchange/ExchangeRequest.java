package com.example.crypto.exchange;

public class ExchangeRequest {
    private String from;
    private String[] to;
    private float amount;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String [] getTo() {
        return to;
    }

    public void setTo(String [] to) {
        this.to = to;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

}
