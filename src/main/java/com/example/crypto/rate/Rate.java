package com.example.crypto.rate;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Rate {

    public Rate(){};

    @JsonProperty("rate")
    private String rate;

    @JsonProperty("asset_id_quote")
    private String id;

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
