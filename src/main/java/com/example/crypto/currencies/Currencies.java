package com.example.crypto.currencies;

import com.example.crypto.coin.Coin;

import java.util.HashMap;

import static com.example.crypto.coin.CoinApi.getCoin;

public class Currencies {
    private String source;

    private HashMap<String, String> rates = new HashMap();

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public HashMap<String, String> getRates() {
        return rates;
    }

    public void setRates(String[] filter){
        Coin coin = getCoin(source);
        if(filter==null)
            return;
        for(String f : filter){
            if(coin.getRates().containsKey(f)){
                rates.put(f, coin.getRates().get(f).getRate());
            }
            else{
                rates.put(f, "-1");
            }
        }
    }

}
