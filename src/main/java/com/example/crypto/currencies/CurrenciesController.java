package com.example.crypto.currencies;

import com.example.crypto.exchange.ExchangeRequest;
import com.example.crypto.exchange.ExchangeResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/currencies")
public class CurrenciesController {

    @GetMapping(value = "/{currency}")
    public Currencies testCoin(@PathVariable String currency, @RequestParam(required=false) String [] filter) {
        Currencies currencies = new Currencies();
        currencies.setSource(currency);
        currencies.setRates(filter);
        return currencies;
    }

    @PostMapping(value = "/exchange")
    @ResponseBody
    public ExchangeResponse exchange(@RequestBody ExchangeRequest exchangeRequest){
        ExchangeResponse exchangeResponse= new ExchangeResponse() ;
        exchangeResponse.calculateExchange(exchangeRequest);
        return exchangeResponse;
    }
}
