package com.example.crypto.coin;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


public class CoinApi {
    public static Coin getCoin(String currency){

        String url = "https://rest.coinapi.io/v1/exchangerate/"+currency;
        HttpHeaders headers = new HttpHeaders();
        headers.set("authorization", "52186701-2203-4B13-80E9-1E77C64BFDB2");
        HttpEntity request = new HttpEntity(headers);
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, HttpMethod.GET, request, Coin.class).getBody();

    }

}
