package com.example.crypto.coin;

import org.springframework.web.bind.annotation.*;

import static com.example.crypto.coin.CoinApi.getCoin;


@RestController
    @RequestMapping(value = "/coin")
    public class CoinController {

        @GetMapping(value = "/{currency}")
        public Coin getAll(@PathVariable String currency){
            return getCoin(currency);
        }

}
