package com.example.crypto.coin;

import com.example.crypto.rate.Rate;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;

@Component
public class Coin {

   @JsonProperty("asset_id_base")
    private String id;

    @JsonProperty("rates")
    private HashMap<String, Rate> rates = new HashMap();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HashMap<String, Rate> getRates() {
        return rates;
    }

    public void setRates(Rate [] arr) {
        for (Rate r : arr) {
            rates.put(r.getId(), r);
        }
    }

    public Coin getSpecificRates(String [] filter) {
        Coin c = new Coin();
        c.id = this.id;
        for (String s : filter) {
            if (this.rates.containsKey(s)) {
                c.rates.put(s, this.rates.get(s));
            }
        }
        return c;
    }

    public Coin getSpecificRates(String filter){
        return getSpecificRates(filter.split("&"));
    }

}
