# crypto-exchange

This application uses external API data to calculate current cryptocurrency rates.

## Description:
- [ ] [Api Used - CoinApi](https://www.coinapi.io/)

Application produces three endpoints:
- **/currencies/{currency}** - where you get filtered rates using query **?filter=** to obtain rates for a given *{currency}*
- **/currencies/exchange** - where you get list of certain exchanges for a given currency
- **/coin/{currency}** - where you get list of all available rates for a given *{currency}*
### Examples:
#### 1.
**Request:** *GET /currencies/BTC?filter=ETH&filter=PLN*

**Response:**

```json
{
	"source": "BTC",
	"rates": {
		"PLN": "170908.59351615774337952381838",
		"USD": "42474.322351428637275855431719",
		"ETH": "14.482178936501926931211089512"
	}
}
```

#### 2.
**Request:** *POST /currencies/exchange*

**RequestBODY:** {"from":"BTC", "to":["ETH","PLN"], "amount":100}

**Response:**

```json
{
	"from": "BTC",
	"exchange": {
		"PLN": {
			"rate": 171026.86,
			"amount": 100.0,
			"result": 1.7273712E7,
			"fee": 171026.86
		},
		"ETH": {
			"rate": 14.472588,
			"amount": 100.0,
			"result": 1461.7313,
			"fee": 14.472588
		}
	}
}
```

#### 3.
**Request:** *GET /coin/BTC*

**Response:**

```json
{
	"asset_id_base": "BTC",
	"rates": {
		"AGLD": {
			"rate": "39535.210753620348469724828947",
			"asset_id_quote": "AGLD"
		},
		"SCP": {
			"rate": "38323.647730802280685845559539",
			"asset_id_quote": "SCP"
		},
		"BBC": {
			"rate": "4878047.0438436718779782782039",
			"asset_id_quote": "BBC"
		}, ...
```
